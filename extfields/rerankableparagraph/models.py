# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import datetime
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from wioframework.amodels import *
import wioframework.fields as models
from wioframework import decorators as dec
from django.conf import settings
from wioframework import gen2

MODELS = []


# @wideio_likeable


@wideio_owned()
@wideiomodel
class RerankableParagraphVote(models.Model):
    vote_for = models.ForeignKey('RerankableParagraph')

    def on_add(self, request):
        pass


def wideio_kudosweight(kudos):
    # TODO FIXME currently assumes that triggers are manually implemented
    # could be added dynamically via on_* override functions
    def trigger(self, event, request):
        for fn in self.events[event]:
            fn(self, request)

    def bind(self, event, fn):
        self.events[event].append(fn)

    def inc_kudos(self, request):
        request.user.get_extended_profile().increase_kudos(self._kudosweight)

    def dec_kudos(self, request):
        request.user.get_extended_profile().decrease_kudos(self._kudosweight)

    def dec(tc):
        tc._kudosweight = kudos
        tc.events = {'add': [inc_kudos], 'delete': [dec_kudos], 'update': []}
        setattr(tc, 'trigger', trigger)
        setattr(tc, 'bind', bind)
        return tc

    return dec


@wideio_publishable()
@wideio_owned()
@wideio_commentable()
@wideio_timestamped
@wideiomodel
@wideio_kudosweight(250)
class RerankableParagraph(models.Model):
    priority = models.IntegerField(default=0)
    field_name = models.CharField(max_length=128, db_index=True)
    content = models.TextField(
        max_length=20000,
        app_null=False,
        db_null=False,
        app_blank=False,
        db_blank=False,
        blank=False)
    # gpk_type = models.ForeignKey(ContentType, null=True,
    #                              verbose_name='content type',
    #                              related_name="content_type_set_for_%(class)s")
    gpk_pk = models.TextField('object ID', null=True, db_index=True)
    generic_pk = gen2.GenericForeignKey2(
        #ct_field="gpk_type",
        fk_field="gpk_pk")

    def get_all_references(self):
        return []

    def other_rerankable_paragraphs(self):
        return filter(lambda x: x.id != self.id,
                      RerankableParagraph.objects.filter(gpk_pk=self.gpk_pk, wiostate='V', field_name=self.field_name))

    def on_add(self, request):
        self.trigger('add', request)
        ##
        ## email other in the stacks
        ## 
        from wioframework.utils import wio_send_email
        for x in self.other_rerankable_paragraphs():
            wio_send_email(request,
                           None,
                           "There is new a contibution to " + self.field_name,
                           "static/email_contribution.html",
                           {"x": self, "content": self.content, "author": self.owner},
                           [x.owner])
        return {'_redirect': self.get_view_url()}

    def on_delete(self, request):
        obj = self.generic_pk
        if obj != None:
            vurl = self.generic_pk.get_view_url()
        else:
            print self.__dict__
            vurl = "/"
        self.trigger('delete', request)
        return {'_redirect': vurl}

    class WIDEIO_Meta:
        DISABLE_VIEW_ACCOUNTING = 1
        permissions = dec.perm_write_for_logged_users_only
        allow_search = [
            'gpk_type',
            'content__icontains',
            'content_istartswith']

        # form_exclude = ['gpk_pk', 'gpk_type']

        class Actions:
            @wideio_action(icon='icon-edit', possible=lambda o, r: (
            (r.user is not None) and (r.user.is_authenticated()) and (r.user != o.owner)), mimetype="text/html")
            def improve_answer(self, request):
                # FIXME: VOTING SHOULD ONLY BE POSSIBLE IF VOTE HAS NOT BEEN
                # EXPRESSED BEFORE
                rpv = RerankableParagraphImprovement()
                rpv.owner = request.user
                rpv.p = self
                rpv.new_content = self.content
                rpv.save()
                return {'_redirect': rpv.get_update_url()}

            @wideio_action(icon="icon-thumbs-down",
                           possible=lambda o, r: ((r.user is not None) and (r.user.is_authenticated())
                                                  and (RerankableParagraphVote.objects.filter(owner=r.user,
                                                                                              vote_for_id=o.id).count() != 0)))
            def vote_down(self, request):
                # FIXME: VOTING DOWN SHOULD ONLY BE POSSIBLE IF VOTE NOT BEEN
                # EXPRESSED BEFORE
                RerankableParagraphVote.objects.filter(
                    owner=request.user,
                    vote_for_id=self.id).delete()
                self.priority = self.priority - 1
                self.save()

                return ""

            @wideio_action(icon="icon-thumbs-up",
                           possible=lambda o, r: ((r.user is not None) and (r.user.is_authenticated())
                                                  and (RerankableParagraphVote.objects.filter(owner=r.user,
                                                                                              vote_for_id=o.id).count() == 0)))
            def vote_up(self, request):
                # FIXME: VOTING SHOULD ONLY BE POSSIBLE IF VOTE HAS NOT BEEN
                # EXPRESSED BEFORE
                rpv = RerankableParagraphVote()
                rpv.owner = request.user
                rpv.vote_for_id = self.id
                rpv.save()
                self.priority = self.priority + 1
                self.save()
                return ""


@wideio_moderated(moderated_by=lambda o: o.p.owner)
@wideio_owned()
@wideio_timestamped
@wideiomodel
class RerankableParagraphImprovement(models.Model):
    p = models.ForeignKey('RerankableParagraph')
    new_content = models.TextField(
        max_length=20000,
        app_null=False,
        db_null=False,
        app_blank=False,
        db_blank=False,
        blank=False)

    def on_moderation_accept(self, request):
        request.user.get_extended_profile().increase_kudos(10)
        self.owner.get_extended_profile().increase_kudos(200)
        self.p.content = self.new_content
        self.delete()

    class WIDEIO_Meta:
        DISABLE_VIEW_ACCOUNTING = 1
        form_exclude = ['p']
        permissions = dec.perm_write_for_logged_users_only
        allow_search = [
            'content__icontains',
            'content_istartswith']
        # form_exclude = ['gpk_pk', 'gpk_type']


MODELS.append(RerankableParagraph)
MODELS += get_dependent_models(RerankableParagraph)

MODELS.append(RerankableParagraphVote)
MODELS += get_dependent_models(RerankableParagraphVote)

MODELS.append(RerankableParagraphImprovement)
MODELS += get_dependent_models(RerankableParagraphImprovement)
